
from django.db.models.base import Model as Model
from django.db.models.query import QuerySet
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse_lazy
from django import forms
from .forms import UserCreationFormWithEmail
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import Profile
from .forms import ProfileForm, EmailForm

class SignUpView(CreateView):
    form_class = UserCreationFormWithEmail
    template_name = 'registration/signup.html'

    def get_success_url(self):
        return reverse_lazy('login') + '?register'
    
    def get_form(self, form_class=None):
        form = super(SignUpView, self).get_form()
        form.fields['username'].widget.attrs = {'class': 'form-control mb-2', 'placeholder': 'Username'}
        form.fields['email'].widget.attrs = {'class': 'form-control mb-2', 'placeholder': 'Email'}
        form.fields['password1'].widget.attrs = {'class': 'form-control mb-2', 'placeholder': 'Password'}
        form.fields['password2'].widget.attrs = {'class': 'form-control mb-2', 'placeholder': 'Password confirmation'}
        return form

@method_decorator(login_required, name='dispatch')
class ProfileUpdateView(UpdateView):
    form_class = ProfileForm
    template_name = 'registration/profile_update.html'
    success_url = reverse_lazy('profile')

    def get_object(self):

        return Profile.objects.get_or_create(user=self.request.user)[0]

@method_decorator(login_required, name='dispatch')
class EmailUpdateView(UpdateView):
    form_class = EmailForm
    template_name = 'registration/profile_email_update.html'
    success_url = reverse_lazy('profile')

    def get_object(self):
        return self.request.user
    
    def get_form(self, form_class=None):
        form = super(EmailUpdateView, self).get_form()
        form.fields['email'].widget.attrs = {'class': 'form-control mb-2', 'placeholder': 'Email'}
        return form