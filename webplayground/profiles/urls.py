from django.urls import path
from .views import ProfileView, DetailProfileView

profiles_urlpatterns = ([
    path('', ProfileView.as_view(), name='list'),
    path('<username>/', DetailProfileView.as_view(), name='detail'),
], 'profiles')