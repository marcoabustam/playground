from django.db.models.base import Model as Model
from django.db.models.query import QuerySet
from django.shortcuts import render, get_object_or_404
from django.views.generic.list import ListView
from registration.models import Profile
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy

# Create your views here.

class ProfileView(ListView):
    model = Profile
    template_name = 'profile/profile_list.html'
    paginate_by = 3


class DetailProfileView(DetailView):
    model = Profile
    template_name = 'profile/profile_detail.html'

    def get_object(self):
        return get_object_or_404(Profile, user__username=self.kwargs['username'])
